import React from 'react';
import './Search-Box.css';

const SearchBox = ({placeholder, handleOnChange}) => {
	return (	<input
	className='search'
		type="search"
		placeholder={placeholder}
		onChange={handleOnChange}
	/>)

};

export default SearchBox;
